npm comes with a command to package node modules into a .tgz file.

```
$ npm pack
```

This creates a .tgz file in the directory it was called in. To test this you can install it another project. In the other project:

```
$ npm install /home/hpierce/phpstorm/ilga-scraper/ilga-scraper-0.0.1.tgz
```

