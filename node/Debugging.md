By default, node scripts run without any debugging support.

```
# no debugging!
node index.js
```

If you pass the `--inspect` flag, then node will run the script with debugging enabled. Node will listen for a debuging client
to connect to an endpoint that it spits out. The endpoint looks like: 

```
ws://127.0.0.1:9229/0f2c936f-b1cd-4ac9-aab3-f63b0f33d55e.
```

The link isn't super important. All the debugging clients I've used are somehow able to magic detect it on their own.


Chrome:

1. Go to chrome://inspect
2. Node will pop up on the screen.
3. Click "Inspect"
4. Add a new workspace contianing the node script.
5. Add breakpoints.

PHPStorm:

1. Add a new configuration called "Attach to Node.js/Chrome".
2. Click it each time the node script reloads.

https://nodejs.org/en/docs/guides/debugging-getting-started/

By default, node only listens for debugging connections on the local machine. To enable remote debugging from any host,
--inspect=0.0.0.0. This is useful for docker containers. For a webserver running in a docker container, running the webserver with

```
node --inspect=0.0.0.0 server.js
```

is sufficient. You attach your debugger at any point while the server is running. If you're running a script that will finish before you get a chance to 
connect a debugger, you can force node to wait for the debugging connection before beginning the script with

```
node --inspect-brk=0.0.0.0 script.js
```