Start a container (downloading and building the image if it doesn't exist already) and run bash.

i: interactive
t: terminal

```
docker run -it [image] [process]
```

docker run -it ubuntu /bin/bash

List running containers

a: show stopped containers

docker ps -a

STart a container

docker start [id]

Question: How does the container know what process to run?

Creates a terminal to a running container

docker attach [id]

Delete an image

docker rm [id]

Volumes: A way to mount host files in the container. Files create in the container are sent back and presumably files on the host are made available in the container.

-v [host]:[guest]


# Examples

## Create a container from the latest `node` image, leave it running in the background:

```
docker run -t -d node
```

`-t`: Assign a terminal to the container.

`-d`: Detach from the container (return to the host's terminal)

Get the id of the container:

```
docker container ls
```

Get into the container:

```
docker exec -it [id] /bin/bash
```

docker-compose.yml equivalent

```
services:
  ...
  ilga-parser:
    image: "node"
    tty: true
    stdin_open: true
```
